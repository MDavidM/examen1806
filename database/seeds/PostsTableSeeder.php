<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // $now = \Carbon\Carbon::now();
        $now = $faker->dateTimeBetween($startDate = '-3 months', $endDate = 'now', $timezone = null);
        // dd($faker->paragraph(   ));
        for ($i=1; $i < 10; $i++) { 
            $user_id = 1 + $i % 2;
            $user2_id = 1 + (1 + $i) % 2;
            // dd($user_id);
            DB::table('posts')->insert([
                'title' => 'Post nº. ' . ($i + 1),
                'content' => $faker->text(300),
                'user_id' => $user_id,
                'date' => $now
            ]);   
            
            DB::table('comments')->insert([
                'text' => $faker->paragraph(),
                'post_id' => ($i),
                'user_id' => $user2_id,
            ]);   
            
        }
    }
}
