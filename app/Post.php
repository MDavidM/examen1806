<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['id', 'title', 'content', 'user_id', 'date'];

    public function user()
    {
        return $this->belongsTo('App\User');
        //return $this->belongsTo(User::class);
    }

    /*public function comment()
    {
        return $this->hasMany('App\Comment');
        //return $this->belongsTo(Module::class);
    }*/
}
