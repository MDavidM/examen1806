@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <h1>{{ $post->title }}</h1>
      @if (Session::has('post'))
      Me gusta!! {{ Session::get('post')->title }}
      <inputtype="submit" class="btn btn-warning" href="/posts/forget">Decir que no me gusta</a>
      @endif
      <a class="btn btn-success" href="/posts/{{$post->id }}/remember">Decir que me gusta</a>
      <br>
      {{ $post->content}}
      @can('edit', $post)
        <form method="post" action="/posts/{{$post->id}}">
          {{csrf_field()}}
          <input type="hidden" name="_method" value="edit">
          <input type="submit" value="borrar {{$post->id}}" class="glyphicon glyphicon-info">
        </form>
      @endcan
      <br><br>
      Publicado por: {{ $post->user->name }} {{ $post->date }}
      <br><br><h2>Comentarios</h2>
      @foreach($comments as $comment)
        {{$comment->user->name}}: {{$comment->text}}<br>
      @endforeach
      <div class="form-group">
        <label>Nuevo comentario</label>
          <input type="text" class="form-control" name="text" value="{{ old('text') }}">
          @if ($errors->first('text'))
          <script>
            alert('no ha comentado adecuadamente');
          </script>
          <div class="alert alert-danger">
            {{ $errors->first('text') }}
          </div>
          @endif
          <br>
          <input type="submit" class="btn btn-primary" value="Enviar comentario" role="button">
      </div>
    </div>
  </div>
</div>
@endsection
