@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content.center">
    <div class="col-sm-12">
      <h1>Edición de post</h1>
      <form class="form" method="post" action="/post/{{ $post->id }}">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <div class="form-group">
            <label>texto</label>
            <input type="textarea" class="form-control" name="name" value="{{ $post->texto}}">
            @if ($errors->first('text'))
            <div class="alert alert-danger">
                {{ $errors->first('text') }}
            </div>
            @endif
        </div>
        <input type="submit" class="btn btn-primary" role="button">
        </form>
        </div>
    </div>
</div>
@endsection
