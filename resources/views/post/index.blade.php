@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Lista de posts</h1>
      <table  class="table table-striped table-hover">
        <tbody>
          @foreach ($posts as $post)
          <tr>
            <td>{{ $post->title }}</td>
            <td>{{ $post->content }}{{ substr(strip_tags($post->content), 0, 100) }}
            <a href="/posts/{{$post->id }}">...leer más</a></td>
            <td>{{ $post->user->name }}</td>
            <td>{{ $post->date }}</td>
          @endforeach
        </tr>
      </tbody>
    </table>
    {{ $posts->render() }}
  </div>
</div>
</div>
@endsection
